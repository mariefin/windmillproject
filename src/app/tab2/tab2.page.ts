import { Component, ViewChild, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { IonContent } from '@ionic/angular';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
  @ViewChild(IonContent) content: IonContent;
  goal: number;
  goal2: number;
  phone: boolean;
  tv: boolean;
  microwave: boolean;
  laundry: boolean;
  dish: boolean;
  pc: boolean;
  laptop: boolean;
  led: boolean;
  bulb: boolean;
  coffee: boolean;
  fridge:boolean;
  freezer: boolean;
  oven: boolean;
  water: boolean;
  vacuum: boolean;
  toaster: boolean;
  sauna: boolean;
  consoles: boolean;
  timeRange: string;
  result: number;
  infoBox: boolean;
  info: string;
  monthly: string;
  daily: string;
  weekly: string;
  yearly: string;
  compareR: string;
  warningBox: boolean;
  warning: string;

  constructor(private storage: Storage) {
    this.result = 0;
    this.infoBox = false;
    this.info = "";
    this.compareR = "";
    this.warningBox = false;
    this.warning = "";
  }
  ngOnInit() {
    this.getGoal();
  }

  getGoal() {
    this.storage.get('goal').then((val) => {
      this.goal = val;
      console.log('Your goal is ', val);
    });
  }
  addGoal() {
    if (this.goal) {
      this.storage.set('goal', this.goal);
      this.warningBox = false;
    } else {
      this.warningBox = true;
      this.warning = "You didn't set your goal!";
    }
    
  }

  getWatts(check,watts) {
    if (check == true) {
      this.result+=watts;
    }

  }
  getCurrent() {
    console.log(this.timeRange);
    this.result = 0;

    this.getWatts(this.phone,0.004);
    this.getWatts(this.tv,0.39);
    this.getWatts(this.microwave,0.2);
    this.getWatts(this.laundry,1.8);
    this.getWatts(this.dish,0.7);
    this.getWatts(this.pc,0.45);
    this.getWatts(this.laptop,0.09);
    this.getWatts(this.led,0.3);
    this.getWatts(this.bulb,0.36);
    this.getWatts(this.coffee,0.24);
    this.getWatts(this.fridge,0.5);
    this.getWatts(this.freezer,0.7);
    this.getWatts(this.oven,0.7);
    this.getWatts(this.water,0.3);
    this.getWatts(this.vacuum,1);
    this.getWatts(this.toaster,0.3);
    this.getWatts(this.sauna,16);
    this.getWatts(this.consoles,0.36);
    
    if (this.result != 0) { 
    this.daily = this.result.toFixed(3);
    this.monthly = (this.result*30).toFixed(3);
    this.weekly = (this.result*7).toFixed(3);
    this.yearly = (this.result*365).toFixed(3);
    this.infoBox = true;
    this.info = "Your daily usage is " + this.daily + "kW. Your weekly usage is " + this.weekly + "kW. Your monthly usage is " + this.monthly + "kW. Your yearly usage is " + this.yearly + "kW.";  
    this.ScrollToTop();
    this.compare();
    } else {
      this.warningBox = true;
      this.warning = "You didn't choose anything!";
      this.ScrollToTop();
      
    }
  }
  ScrollToTop(){
    this.content.scrollToTop(1500);
  }
  compare() {
    this.getGoal();
    let compareN = this.result*30;

    if (this.goal >= compareN) {
      this.compareR = "Congratulations! You have met your goal!";
    } else {
      this.compareR = "You haven't met your monthly goal yet. Keep on saving! You are doing well!";
    }

  }
  
}
