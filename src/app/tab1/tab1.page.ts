import { Component, OnInit, ViewChild } from '@angular/core';
import { WindService } from '../wind.service';
import { Router } from '@angular/router';
import { Chart } from 'chart.js';
import * as moment from 'moment';
import { NavController } from '@ionic/angular';
import { IonContent } from '@ionic/angular';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  @ViewChild('barCanvas') barCanvas;
  @ViewChild('lineCanvas') lineCanvas;
  @ViewChild(IonContent) content: IonContent;
  wind: any;
  barChart: any;
  lineChart: any;
  windPower = [];
  power = [];
  waterPower = [];
  windChart = [];
  waterChart = [];
  time = [];
  chart: boolean;
  searchB: boolean;
  startDate: string;
  startTime: string;
  endDate: string;
  endTime: string;
  powerType: string;
  timeType:string;
  searchUrl: string;
  searchData = [];
  searchChart = [];
  warning : string;
  warningBox : boolean;
  isEnabled: boolean;
  type: string;

  constructor (private windService: WindService, private router: Router, private navController : NavController) {
    this.chart = false;
    this.searchB = false;
    this.startTime = "00:00";
    this.endTime = "00:00";
    this.powerType ="wind";
    this.timeType ="real";
    this.warningBox = false;
    this.warning = "";
    this.isEnabled = false;
  }

  ngOnInit() {
    this.getData('v1/variable/181/event/json');
  }

  getData(urli) {
    this.windService.getData(urli).subscribe(data => {
      console.log(data);
      this.wind = data;
    })
    
  }
  getChartData(arr, pusharr) {
    for (let i = 0; i<arr.length; i++) {
      pusharr.push(arr[i].value);
    }
  }
  getChartLabels(arr) {
    for (let i = 0; i<arr.length; i++) {
      let num = moment(arr[i].start_time).utcOffset(2).format("DD.MM.YYYY, HH:mm");
      this.time.push(num);
    }
  }

  getPower() {
   
    this.time = [];
    this.chart = true;
    this.searchB = false;
    let today = moment().format("YYYY-MM-DD");
    let yesterday = moment().subtract(1, 'days').format("YYYY-MM-DD");
    
    let hour = moment().format("HH");
    let min = moment().format("mm");
    let windUrl: string = 'v1/variable/181/events/json?start_time='+ yesterday + 'T'+hour+'%3A'+min+'%3A00%2B0200&end_time='+ today + 'T'+hour+'%3A'+min+'%3A00%2B0200';
    let waterUrl: string = 'v1/variable/191/events/json?start_time='+ yesterday + 'T'+hour+'%3A'+min+'%3A00%2B0200&end_time='+ today + 'T'+hour+'%3A'+min+'%3A00%2B0200';

    this.windService.getData(waterUrl).subscribe(data => {
      this.waterChart = data;
    });
    if (this.waterChart) {
    this.windService.getData(windUrl).subscribe(data => {
      console.log(data);
      this.windChart = data;
      this.getChartData(this.windChart, this.windPower);
      this.getChartData(this.waterChart, this.waterPower);
      this.getChartLabels(this.windChart);
      this.getChart(this.time,this.windPower,this.waterPower);
      this.ScrollToBottom();
      });
    }
  }
  ScrollToBottom(){
    this.content.scrollToBottom(1500);
  }

 
 getChart(labels, data1, data2) {
    
      this.barChart = new Chart(this.barCanvas.nativeElement, {
  
              type: 'line',
              data: {
                  labels: labels,
                  datasets: [{
                      label: 'Wind',
                      data: data1,
                      borderColor: 'rgb(58, 95, 196)',
                      backgroundColor: 'rgba(58, 95, 196, 0)',
                      fill:false,
                      borderWidth: 1
                  },
                  {
                    label: 'Water',
                    data: data2,
                    borderColor: 'rgba(0,0,0)',
                    fill:false,
                    borderWidth: 1
                }
                 ],
              },
              options: {
                scales: {
                      yAxes: [{
                          ticks: {
                              beginAtZero:true
                          }
                      }]
                  }
              }
  
          });
  }
  getSearchChart(labels, label, data1) {
    
    this.barChart = new Chart(this.barCanvas.nativeElement, {

            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    label: label,
                    data: data1,
                    borderColor: 'rgb(58, 95, 196)',
                    backgroundColor: 'rgba(58, 95, 196, 0)',
                    fill:false,
                    borderWidth: 1
                },
               ],
            },
            options: {
              scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }

        });
}

  search() {
    console.log(this.endTime);
    console.log(this.endDate);
    this.searchB = false;
    this.time = [];
    this.searchData = [];
    this.searchChart = [];
    let stime = this.startTime.replace(":", "%3A");
    let etime = this.endTime.replace(":", "%3A");
    let type: string;
    let sDate = moment(this.startDate);
    let eDate = moment(this.endDate);

    if (this.powerType == "wind") {
      if (this.timeType == "hourly") {
        type = "74";
      } else {
        type ="181";
      }
    } else if (this.powerType == "water") {
      type ="191";
    }
    
    if (moment().isBefore(sDate) || moment().isBefore(eDate)) {
      this.warningBox = true;
      this.warning = "Your start or end date can't be in the future";
    } else if (this.startDate == this.endDate && this.startTime == this.endTime) {
      this.warningBox = true;
      this.warning = "Your start date and time can't be same with end date and time";
    } else if (moment(eDate).isBefore(sDate)) {
      this.warningBox = true;
      this.warning = "Your end date can't be before start date";
    } else if (this.powerType == "water" && this.timeType == "hourly") {
      this.warningBox = true;
      this.warning = "You can't choose hourly time with water.";
    } else if (this.endTime.length < 1 || this.startTime.length < 1) {
      this.warningBox = true;
      this.warning = "End or Start time cannot be empty";
    } else if (this.startDate === undefined || this.startDate === "") {
      this.warningBox = true;
      this.warning = "Start date cannot be undefined";
    } else if (this.endDate === undefined || this.endDate === "") {
      this.warningBox = true;
      this.warning = "End date cannot be undefined";
    }else {
      this.warningBox = false;
      this.searchB = true;
      this.searchUrl = 'v1/variable/'+type+'/events/json?start_time='+ this.startDate + 'T'+stime+'%3A00%2B0200&end_time='+ this.endDate + 'T'+etime+'%3A00%2B0200';
      this.chart = false;
      if (this.timeType == "hourly") {
        this.type = "MWh/h";
      } else {
        this.type = "MW";
      }
      
      this.windService.getData(this.searchUrl).subscribe(data => {
        this.searchData = data;
        this.getChartData(this.searchData, this.searchChart);
        this.getChartLabels(this.searchData);
        this.getSearchChart(this.time,this.powerType,this.searchChart);
        this.ScrollToBottom();
      });
    }

  }
  
  


}
