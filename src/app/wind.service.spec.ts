import { TestBed } from '@angular/core/testing';
import { HttpClientModule} from '@angular/common/http';

import { WindService } from './wind.service';

describe('WindService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule],
  }));

  it('should be created', () => {
    const service: WindService = TestBed.get(WindService);
    expect(service).toBeTruthy();
  });
});
