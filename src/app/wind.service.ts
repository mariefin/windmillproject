import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = environment.apiUrl;
const API_KEY = environment.apiKey;

@Injectable({
  providedIn: 'root'
})
export class WindService {

  headers2 = new HttpHeaders().set("x-api-key", API_KEY);

  constructor(private http: HttpClient) { }
  
  getData(url): Observable<any> {
    var address = `${API_URL}/${url}`;
    console.log(address);
    console.log(this.headers2);
    return this.http.get(address, {headers: this.headers2});
  }
}
