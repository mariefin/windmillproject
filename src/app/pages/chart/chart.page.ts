import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Chart } from 'chart.js';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.page.html',
  styleUrls: ['./chart.page.scss'],
})
export class ChartPage implements OnInit {

  @ViewChild('barCanvas') barCanvas;
  barChart: any;
  url: string;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.url = this.activatedRoute.snapshot.paramMap.get('searchUrl');
    console.log(this.url);
  }

  getChart() {
    
    this.barChart = new Chart(this.barCanvas.nativeElement, {

            type: 'line',
            data: {
                labels: [1,2,3,4],
                datasets: [{
                    label: 'Wind',
                    data: [1,2,3,4],
                    borderColor: 'rgba(255,99,132,1)',
                    backgroundColor: 'rgba(255,99,132,1)',
                    fill:false,
                    borderWidth: 1
                }
               ],
            },
            options: {
              scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }

        });
}

}
